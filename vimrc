set cc=80
set tw=80
set fo+=t
"-----------Global settings-------------
set nocompatible
set laststatus=2
set number
set relativenumber
set autoindent
set shiftwidth=4
set smartindent
set tabstop=4
set clipboard=unnamedplus
set ignorecase

au filetype rust setlocal mp="cargo b"

" Cursor variables
let &t_SI = "\<Esc>[6 q"
let &t_SR = "\<Esc>[4 q"
let &t_EI = "\<Esc>[2 q"

"--AUTOCOMPLETION--
filetype plugin on
set omnifunc=synctaxcomplete#Complete
inoremap <silent><expr> <CR> coc#pum#visible() ? coc#_select_confirm()
			\: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

if has('nvim')
	inoremap <silent><expr> <c-space> coc#refresh()
else
	inoremap <silent><expr> <c-@> coc#refresh()
endif
" -----------MAPPINGS--------------------
imap jj <Esc>
nmap <C-s> :w <CR>

" C/C++ make and run key mappings 
map <F5> :w <CR> :!make <CR> 
map <F6> :!./%< <CR>

au filetype rust map <F6> :!cargo r <CR>

" buffers not tabs
nnoremap <Tab> bn<CR> 
nnoremap <S-Tab> bp<CR> 
nnoremap <C-t> :enew<Space>

" buffers
" nnoremap <C-b> :e<Space>
" nnoremap <C-k> :bnext<CR>
" nnoremap <C-j> :bprevious<CR>
map gn :bn<cr>
map gp :bp<cr>
map gd :bd<cr>  

map <C-f> :Files<CR>
map <C-p> :FZF<CR>
map <C-g> :Rg<CR>
noremap <silent> <c-k> :.m-2<cr>
noremap <silent> <c-j> :.m+2<cr>


"------------AUTOCOMMANDS-----------------
autocmd BufRead *.ms :silent !zathura %:r.pdf &
autocmd BufWritePost *.ms :!groff -ms -p % -Tps | ps2pdf - %:r.pdf
autocmd FileType rust nnoremap <leader>r :!cargo r <cr>

"------------ENDMAPPINGS-----------------
"
"
" Git branchname in status bar 
function! GitBranch()
  return system("git rev-parse --abbrev-ref HEAD 2>/dev/null | tr -d '\n'")
endfunction


function! StatuslineGit()
  let l:branchname = GitBranch()
  return strlen(l:branchname) > 0?'  '.l:branchname.' ':''
endfunction

if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif
"--------------Vim plugins-----------
"
call plug#begin()
Plug 'https://github.com/tpope/vim-commentary'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'mhinz/vim-startify'
Plug 'arcticicestudio/nord-vim'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
call plug#end()
"
"------------------------------------
"
"--------------startify--------------
let g:ascii = [
			\"											   __",
			\"											  /` ,\__",
			\"											 |    ).-'",
			\"											/ .--'",
			\"                  					   /  /",
			\"                            ,      _.==''`  \\", 
			\"▀████▀                    .'(  _.='         |", 
			\"  ██                     {   ``  _.='       |", 
			\"  ██  ▄█▀██▄ ▀████████▄   {    \`     ;     /", 
			\"  ██ ██   ██   ██    ██    `.   `'=..'  .='", 
			\"  ██  ▄█████   ██    ██      `=._    .='", 
			\"  ██ ██   ██   ██    ██   jgs  '-`\\`__", 
			\"▄████▄████▀██▄████  ████            `-._("
			\]

let g:startify_custom_header ='startify#center(g:ascii)'
let g:startify_bookmarks= ["~/.vimrc", "~/.config/sxhkd", "~/.config/compton/compton.conf", "~/Projects/3dworld"]
let g:startify_session_autoload = 1

"----------------colorscheme-----------------------
set termguicolors
set t_Co=256
colorscheme nord
"--------------------------------------

" --------------StatusLine--------------
" set background=dark    " Setting dark mode
set statusline=2
set statusline+=%#PmenuSel#
set statusline+=%{StatuslineGit()}
set statusline+=%#LineNr#
set statusline+=\ %f
set statusline+=%m\
set statusline+=%=
set statusline+=%#DiffDelete#
set statusline+=\ %y
set statusline+=\ %{&fileencoding?&fileencoding:&encoding}
set statusline+=\[%{&fileformat}\]
set statusline+=\ %p%%
set statusline+=\ %l:%c
set statusline+=\ 

"
" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window.
nnoremap <silent> K :call ShowDocumentation()<CR>

function! ShowDocumentation()
  if CocAction('hasProvider', 'hover')
    call CocActionAsync('doHover')
  else
    call feedkeys('K', 'in')
  endif
endfunction
